/* See LICENSE file for copyright and license details. */
#include <dirent.h>
#include <stddef.h>
#include <string.h>

#include "../util.h"


#if defined(__linux__)
	#include <stdint.h>

	const char *
	temp(const char *file)
	{
		uintmax_t temp;

		if (pscanf(file, "%ju", &temp) != 1) {
			return NULL;
		}

		return bprintf("%ju", temp / 1000);
	}

#define THERMAL_DIR "/sys/class/thermal/"
#define TEMP_FILE "/temp"
const char *
max_temp(const char *unused)
{
	struct dirent *dp;
	DIR *fd;
	size_t len;
	uintmax_t temp;
	uintmax_t max_temp = 0;
	char file[512] = THERMAL_DIR;

	(void)(unused);

	if (!(fd = opendir(THERMAL_DIR))) {
		warn("opendir '%s':", THERMAL_DIR);
		return NULL;
	}
	while ((dp = readdir(fd))) {
		if (strncmp(dp->d_name, "thermal_zone", sizeof("thermal_zone") - 1))
			continue;
		len = strlen(dp->d_name);
		strcpy(file + sizeof THERMAL_DIR - 1, dp->d_name);
		strcpy(file + sizeof THERMAL_DIR - 1 + len, TEMP_FILE);
		if (pscanf(file, "%ju", &temp) != 1)
			continue;
		if (temp > max_temp)
			max_temp = temp;
	}

	closedir(fd);

	if (!max_temp)
		return NULL;
	return bprintf("%ju", max_temp / 1000);
}
#elif defined(__OpenBSD__)
	#include <stdio.h>
	#include <sys/time.h> /* before <sys/sensors.h> for struct timeval */
	#include <sys/sensors.h>
	#include <sys/sysctl.h>

	const char *
	temp(const char *unused)
	{
		int mib[5];
		size_t size;
		struct sensor temp;

		mib[0] = CTL_HW;
		mib[1] = HW_SENSORS;
		mib[2] = 0; /* cpu0 */
		mib[3] = SENSOR_TEMP;
		mib[4] = 0; /* temp0 */

		size = sizeof(temp);

		if (sysctl(mib, 5, &temp, &size, NULL, 0) < 0) {
			warn("sysctl 'SENSOR_TEMP':");
			return NULL;
		}

		/* kelvin to celsius */
		return bprintf("%d", (temp.value - 273150000) / 1E6);
	}
#elif defined(__FreeBSD__)
	#include <stdio.h>
	#include <stdlib.h>
	#include <sys/sysctl.h>

	const char *
	temp(const char *zone)
	{
		char buf[256];
		int temp;
		size_t len;

		len = sizeof(temp);
		snprintf(buf, sizeof(buf), "hw.acpi.thermal.%s.temperature", zone);
		if (sysctlbyname(buf, &temp, &len, NULL, 0) == -1
				|| !len)
			return NULL;

		/* kelvin to decimal celcius */
		return bprintf("%d.%d", (temp - 2731) / 10, abs((temp - 2731) % 10));
	}
#endif
