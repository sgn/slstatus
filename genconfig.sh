#!/bin/sh

cat <<EOF
/* See LICENSE file for copyright and license details. */

/* interval between updates (in ms) */
const unsigned int interval = 1000;

/* text to show if no value can be retrieved */
static const char unknown_str[] = "n/a";

/* maximum output string length */
#define MAXLEN 2048

EOF

cat <<EOF
static const struct arg args[] = {
	/* function format          argument */
EOF

for dir in /sys/class/net/wl*
do
	if test -d "$dir"
	then
		cat <<EOF
	{ wifi_essid, " W: %s ",     "${dir##*/}" },
	{ wifi_perc,  "%s%% |",      "${dir##*/}" },
EOF
	fi
done

cat <<EOF
	{ vol_perc, " ♪ %s |",        "Master" },
	{ cpu_perc, " C: %s%%,",      NULL },
	{ max_temp, " %s℃ |",         NULL },
EOF

# Winner of worst code ever
bat_marker=" BAT:"
bat_padding=
bat=
for dir in /sys/class/power_supply/BAT*
do
	test -z "$bat" ||
	cat <<EOF
	{ battery_state, "%s,",       "$bat" },
EOF
	if test -d "$dir"
	then
		bat=${dir##*/}
		cat <<EOF
	{ battery_perc, "$bat_marker %s%%",$bat_padding "$bat" },
EOF
	fi
	bat_marker=
	bat_padding='     '
done
test -z "$bat" ||
cat <<EOF
	{ battery_state, "%s |",      "$bat" },
EOF

cat <<EOF
	{ datetime, " %s ",           "%T" },
};
EOF
